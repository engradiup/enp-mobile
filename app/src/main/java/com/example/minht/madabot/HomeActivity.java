package com.example.minht.madabot;

import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.os.Handler;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.TabLayout;
import android.support.v7.app.AppCompatActivity;

import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;

import com.example.minht.madabot.adapters.SectionPageAdapter;

import java.util.Calendar;

import static android.app.PendingIntent.getActivity;

public class HomeActivity extends AppCompatActivity implements TabLayout.OnTabSelectedListener {

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    private ViewPager mViewPager;
    private SectionPageAdapter mSectionPageAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_list);
        mSectionPageAdapter = new SectionPageAdapter(getSupportFragmentManager());

        mViewPager = (ViewPager) findViewById(R.id.container);
        init(mViewPager);

        TabLayout tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(mViewPager);
        onButtonClicked();

        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.profile);
        Calendar c = Calendar.getInstance();
        if (c.get(Calendar.HOUR_OF_DAY) > 18) {
            linearLayout.setBackgroundColor(Color.parseColor("#1CA5B8"));

        } else {
            linearLayout.setBackgroundColor(Color.parseColor("#FFFFFF"));

        }

        ((ImageView) findViewById(R.id.prof)).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Uri gmmIntentUri = Uri.parse("geo:0,0?q=school");
                        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
                        mapIntent.setPackage("com.google.android.apps.maps"); // build in google map
                        startActivity(mapIntent);
                    }
                }, 1000);
            }
        });



    }

    public void init(ViewPager viewPager) {

        SectionPageAdapter adapter = new SectionPageAdapter(getSupportFragmentManager());
        adapter.addFragment(new ModuleListFragment(), "Modules");
        adapter.addFragment(new StatsFragment(), "Stats");
        adapter.addFragment(new CheatsheetFragment(), "Profile");
        viewPager.setAdapter(adapter);
    }


    @Override
    public void onTabSelected(TabLayout.Tab tab) {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.container, new ModuleListFragment(), "fragment_tag").commit();
    }

    @Override
    public void onTabUnselected(TabLayout.Tab tab) {

    }

    @Override
    public void onTabReselected(TabLayout.Tab tab) {

    }


    String editText;
    Spinner difficulty;
    private FloatingActionButton add_button;

    private void onButtonClicked() {
        add_button = (FloatingActionButton) findViewById(R.id.fab);
        add_button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                NewChatFragment dialog = new NewChatFragment();
                dialog.show(getSupportFragmentManager(), "New Chat Fragment");

            }
        });
    }


}
