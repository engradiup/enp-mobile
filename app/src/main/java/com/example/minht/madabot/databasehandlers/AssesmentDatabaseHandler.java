package com.example.minht.madabot.databasehandlers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.minht.madabot.model.StudentAssessment;
import com.example.minht.madabot.model.StudentModule;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * Created by minht on 12/9/2017.
 */

public class AssesmentDatabaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 6;

    private static final String DATABASE_NAME = "enp";

    private static final String KEY_ID = "assessmentId";
    private static final String KEY_MODULE_ID = "moduleId";
    private static final String KEY_NAME = "assesmentName";
    private static final String KEY_ROOM = "room";
    private static final String KEY_SCHEDULED = "scheduledDate";
    private static final String KEY_TAKEN = "takenDate";
    private static final String KEY_WEIGHT = "weight";
    private static final String KEY_SCORE = "score";

    


    public AssesmentDatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_ASSESSMENT_TABLE = "CREATE TABLE assessments("
                + KEY_ID + " TEXT PRIMARY KEY, "
                + KEY_MODULE_ID + " TEXT, "
                + KEY_NAME + " TEXT, "
                + KEY_ROOM + " TEXT, "
                + KEY_SCHEDULED + " TEXT, "
                + KEY_TAKEN + " TEXT, "
                + KEY_WEIGHT + " REAL, "
                + KEY_SCORE + " REAL "
                +")";
        db.execSQL(CREATE_ASSESSMENT_TABLE);
        Log.d("created","1");

    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS assessments" );

        // Create tables again
        onCreate(db);
    }


    public  void addAssessment(StudentAssessment studentAssessment) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID, studentAssessment.getAssessmentId());
        values.put(KEY_MODULE_ID, studentAssessment.getModuleId());
        values.put(KEY_NAME, studentAssessment.getAssesmentName());
        values.put(KEY_ROOM, studentAssessment.getRoom());
        values.put(KEY_SCHEDULED, getDateString(studentAssessment.getScheduledDate()));
        values.put(KEY_TAKEN, getDateString(studentAssessment.getTakenDate()));
        values.put(KEY_WEIGHT, studentAssessment.getWeight());
        values.put(KEY_SCORE, studentAssessment.getScore());
        // Inserting Row

        db.insert("assessments",null,values);

        db.close(); // Closing database connection
    }

    public  void update(StudentAssessment studentAssessment) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_SCORE, studentAssessment.getScore());
        // Inserting Row

        db.update("assessments",values,"assessmentId =?",new String[] { studentAssessment.getAssessmentId() });

        db.close(); // Closing database connection
    }



    String getDateString(Date date){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm");

        if (date==null){

            return sdf.format(new Date());
        }
        return sdf.format(date);
    }
    Date getDate(String date){
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:MM");

        try {
            return sdf.parse(date);
        } catch (ParseException e) {
            e.printStackTrace();
            return null;
        }
    }

    String getString(String[] s){
        String temp="";
        if (s != null && s.length>0 ){
            for (String st:s){
                temp=temp+ (s+",");

            }
        } else {return "";}
        return temp.substring(0,temp.length()-1);
    }

    public List<StudentAssessment> getAll(){


        List<StudentAssessment> moduleList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM assessments";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                StudentAssessment topic=StudentAssessment.builder()
                        .assessmentId(cursor.getString(0))
                        .moduleId(cursor.getString(1)).assesmentName(cursor.getString(2))
                        .room(cursor.getString(3))
                        .scheduledDate(getDate(cursor.getString(4)))
                        .takenDate(getDate(cursor.getString(5)))
                        .weight(cursor.getDouble(6))
                        .score(cursor.getDouble(7))
                        .build();
                // Adding contact to list
                moduleList.add(topic);
            } while (cursor.moveToNext());
        }

        // return contact list
        return moduleList;
    }



    public StudentAssessment getByDate(Date date){


    SQLiteDatabase db = this.getReadableDatabase();

    Cursor cursor = db.query("assessments", new String[] { KEY_ID,
                    KEY_MODULE_ID, KEY_NAME,KEY_ROOM,KEY_SCHEDULED,KEY_TAKEN,KEY_WEIGHT,KEY_SCORE }, KEY_SCHEDULED + "=?",
            new String[] { getDateString(date) }, null, null, null, null);
    if (cursor != null) {
        cursor.moveToFirst();
        return null;
    }else{
            StudentAssessment topic=StudentAssessment.builder()
                    .assessmentId(cursor.getString(0))
                    .moduleId(cursor.getString(1)).assesmentName(cursor.getString(2))
                    .room(cursor.getString(3))
                    .scheduledDate(getDate(cursor.getString(4)))
                    .takenDate(getDate(cursor.getString(5)))
                    .weight(cursor.getDouble(6))
                    .score(cursor.getDouble(7))
                    .build();
            return topic;
        }
    }

    public List<StudentAssessment> getByModuleId(String  moduleID){


        SQLiteDatabase db = this.getReadableDatabase();

        Cursor cursor = db.query("assessments", new String[] { KEY_ID,
                        KEY_MODULE_ID, KEY_NAME,KEY_ROOM,KEY_SCHEDULED,KEY_TAKEN,KEY_WEIGHT,KEY_SCORE }, KEY_MODULE_ID + "=?",
                new String[] { moduleID }, null, null, null, null);
        if (cursor != null) {
            cursor.moveToFirst();
        }
            List<StudentAssessment> topic = new ArrayList<>();
            if (cursor.getCount()>0){
                do {

                    // Adding contact to list

                    topic.add(StudentAssessment.builder()
                            .assessmentId(cursor.getString(0))
                            .moduleId(cursor.getString(1)).assesmentName(cursor.getString(2))
                            .room(cursor.getString(3))
                            .scheduledDate(getDate(cursor.getString(4)))
                            .takenDate(getDate(cursor.getString(5)))
                            .weight(cursor.getDouble(6))
                            .score(cursor.getDouble(7))
                            .build());
                } while (cursor.moveToNext());
            }
            return topic;

    }





}
