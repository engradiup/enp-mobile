package com.example.minht.madabot;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.example.minht.madabot.databasehandlers.ModuleDatabaseHandler;

/**
 * Created by minht on 4/26/2018.
 */

public class ViewAssessmentFragment extends DialogFragment {

    private String caTitle;
    private String caweight;
    private String caScore;




    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.AlertDialogCustom));
        // Get the layout inflater
        final LayoutInflater inflater = getActivity().getLayoutInflater();
        ModuleDatabaseHandler moduleDatabaseHandler = new ModuleDatabaseHandler(getActivity());
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout


        final View v= inflater.inflate(R.layout.test_info,null);

        String title = getArguments().getString("title");
        String weight = getArguments().getString("weight");
        String score = getArguments().getString("score");


        TextView v1= (TextView) v.findViewById(R.id.ca_title);
        v1.setText(title);

        TextView v2= (TextView) v.findViewById(R.id.ca_schedule);
        v2.setText(title);

        TextView v3= (TextView) v.findViewById(R.id.ca_venue);
        v3.setText(weight);

        TextView v4= (TextView) v.findViewById(R.id.ca_weight);
        v4.setText(score);



        builder.setView(v)
                // Add action buttons
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // sign in the user ...



                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        ViewAssessmentFragment.this.getDialog().cancel();
                    }
                });

        return builder.create();
    }


}
