package com.example.minht.madabot;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;

import com.example.minht.madabot.databasehandlers.AssesmentDatabaseHandler;
import com.example.minht.madabot.model.StudentAssessment;
import com.example.minht.madabot.model.User;
import com.google.gson.Gson;
import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;
import com.loopj.android.http.SyncHttpClient;

import org.json.JSONArray;
import org.json.JSONException;

import java.io.UnsupportedEncodingException;
import java.util.List;

import cz.msebera.android.httpclient.Header;
import cz.msebera.android.httpclient.entity.StringEntity;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        final EditText usernameField = (EditText)findViewById(R.id.usernameInput);
        final EditText passwordField = (EditText)findViewById(R.id.passwordInput);
        final Button signInField = (Button) findViewById(R.id.signInButton);
        final TextView thingy = (TextView) findViewById(R.id.gpaDisplay);

        usernameField.setSelectAllOnFocus(true);
        passwordField.setSelectAllOnFocus(true);


        final ImageButton gpaButton = (ImageButton) findViewById(R.id.gpaButton);
        gpaButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                gpaButton.setPressed(true);

            }
        });
        gpaButton.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {

                switch (event.getAction() & MotionEvent.ACTION_MASK) {

                    case MotionEvent.ACTION_DOWN:
                        v.setPressed(true);
                        // Start action ...
                        usernameField.setVisibility(View.INVISIBLE);
                        passwordField.setVisibility(View.INVISIBLE);
                        signInField.setVisibility(View.INVISIBLE);
                        thingy.setVisibility(View.VISIBLE);
                        break;
                    case MotionEvent.ACTION_UP:
                        usernameField.setVisibility(View.VISIBLE);
                        passwordField.setVisibility(View.VISIBLE);
                        signInField.setVisibility(View.VISIBLE);
                        thingy.setVisibility(View.INVISIBLE);
                        break;
                    case MotionEvent.ACTION_OUTSIDE:
                    case MotionEvent.ACTION_CANCEL:
                        v.setPressed(false);
                        // Stop action ...
                        break;
                    case MotionEvent.ACTION_POINTER_DOWN:
                        break;
                    case MotionEvent.ACTION_POINTER_UP:
                        break;
                    case MotionEvent.ACTION_MOVE:
                        break;
                }


                AssesmentDatabaseHandler assesmentDatabaseHandler = new AssesmentDatabaseHandler(getApplicationContext());
                double sum=0;

                List<StudentAssessment> ss= assesmentDatabaseHandler.getAll();

                for (StudentAssessment s:ss){
                    sum=sum+(s.getScore()*s.getWeight());
                }

                thingy.setText(sum+"");


                return true;
            }
        });
    }

    public void doSignin(View v) throws UnsupportedEncodingException {

        EditText usernameField = (EditText)findViewById(R.id.usernameInput);
        EditText passwordField = (EditText)findViewById(R.id.passwordInput);


        String username=usernameField.getText().toString();
        String password=passwordField.getText().toString();

        if (username.equals("")||password.equals("")){
            Toast.makeText(this, "Enter your username and password", Toast.LENGTH_SHORT).show();

        } else {
            AsyncHttpClient client = new AsyncHttpClient();
            User u = new User();
            Gson gson = new Gson();
            String json = gson.toJson(u);


                Intent intent = new Intent(LoginActivity.this, HomeActivity.class);
                startActivity(intent);

                Toast.makeText(LoginActivity.this, "logged in", Toast.LENGTH_SHORT).show();
                LoginActivity.this.finish();

        }
    }


}
