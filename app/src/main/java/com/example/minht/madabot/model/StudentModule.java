package com.example.minht.madabot.model;


import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by minht on 4/11/2018.
 */

@Data
@AllArgsConstructor(suppressConstructorProperties = true)
@NoArgsConstructor
@Builder

public class StudentModule {

    private String moduleId;
    private String moduleName;
    private String lecturerName;
    private String[] tags;
    private String note;
    private double currentGPA;
}
