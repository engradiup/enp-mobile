package com.example.minht.madabot;

import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.example.minht.madabot.databasehandlers.ModuleDatabaseHandler;
import com.example.minht.madabot.model.StudentModule;

import java.util.ArrayList;

/**
 * Created by minht on 11/11/2017.
 */

public class ModuleListFragment extends Fragment {

    private static final String TAB1="TAB1";
    private View view=null;
    CustomAdapter1 listViewAdapter;
    public static  ArrayList<StudentModule> chat_titles=new ArrayList<>();

    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);
        ModuleDatabaseHandler db = new ModuleDatabaseHandler(getActivity());
        chat_titles= (ArrayList<StudentModule>) db.getAllTopic();
        if (chat_titles.size()==0) {
            db.addModule(StudentModule.builder().moduleId("COMP101").moduleName("Introduction to Programming").currentGPA(80).build());
            db.addModule(StudentModule.builder().moduleId("COMP102").moduleName("Mathematic 1").currentGPA(60).build());
            db.addModule(StudentModule.builder().moduleId("COMP103").moduleName("Spreadsheet Data Analytics").currentGPA(50).build());
            db.addModule(StudentModule.builder().moduleId("COMM101").moduleName("Introduction to web programming").currentGPA(90).build());
            db.addModule(StudentModule.builder().moduleId("COMP105").moduleName("Applied Software Proj Management").currentGPA(70).build());
            Log.d("add","asd");
            chat_titles= (ArrayList<StudentModule>) db.getAllTopic();
        }
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);


    }
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {



        view =inflater.inflate(R.layout.chat_fragment,container,false);
        ListView listView = (ListView) view.findViewById(R.id.list);
        listViewAdapter = new CustomAdapter1(getContext());

        Log.d("color: ",listView.getBackground().toString()+"");
        listView.setAdapter(listViewAdapter);
        listViewAdapter.notifyDataSetChanged();

        return view;

    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);


    }

    class CustomAdapter1 extends BaseAdapter{
        Context context;

        public CustomAdapter1(){

        }

        public CustomAdapter1(Context context){
            this.context=context;

        }
        @Override
        public int getCount() {
            return chat_titles.size();
        }

        @Override
        public Object getItem(int position) {
            return chat_titles.get(position);
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            final ModuleListFragment.CustomAdapter1.ViewHolder holder;
            LayoutInflater vi = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);

            if (convertView == null) {
                convertView = vi.inflate(R.layout.listview_row, null);
                holder = createViewHolder(convertView);
                convertView.setTag(holder);
            } else {
                holder = (ModuleListFragment.CustomAdapter1.ViewHolder) convertView.getTag();
            }


            holder.title.setText(chat_titles.get(position).getModuleName());
            holder.id=chat_titles.get(position).getModuleId();
            holder.description.setText(chat_titles.get(position).getNote());
            holder.title.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent = new Intent(getContext(), ModuleDetailActivity.class);
                    String title= ((TextView) v).getText().toString();
                    intent.putExtra("title",title);
                    intent.putExtra("id",holder.id);
                    intent.putExtra("gpa",String.format("%.0f", chat_titles.get(position).getCurrentGPA()) );
                    intent.putExtra("note",chat_titles.get(position).getNote());
                    intent.putExtra("tags",chat_titles.get(position).getTags());
                    startActivity(intent);
                }
            });
            return convertView;
        }

        private ModuleListFragment.CustomAdapter1.ViewHolder createViewHolder(View v) {
            final ModuleListFragment.CustomAdapter1.ViewHolder holder = new ModuleListFragment.CustomAdapter1.ViewHolder();
            holder.title = (TextView) v.findViewById(R.id.chat_titles);
            holder.description=(TextView) v.findViewById(R.id.chat_des);
            holder.img=(ImageView) v.findViewById(R.id.chatimg);

            return holder;
        }

        public class ViewHolder {
            public TextView title;
            public TextView description;
            public ImageView img;
            public String id;
        }
    }




}
