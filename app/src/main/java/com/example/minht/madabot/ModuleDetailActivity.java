package com.example.minht.madabot;

import android.support.annotation.NonNull;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.applandeo.materialcalendarview.CalendarView;
import com.applandeo.materialcalendarview.EventDay;
import com.applandeo.materialcalendarview.listeners.OnDayClickListener;
import com.example.minht.madabot.databasehandlers.AssesmentDatabaseHandler;
import com.example.minht.madabot.model.StudentAssessment;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

public class ModuleDetailActivity extends AppCompatActivity {

    AssesmentDatabaseHandler assesmentDatabaseHandler;
    private String moduleId;
    private void init(){
        assesmentDatabaseHandler= new AssesmentDatabaseHandler(ModuleDetailActivity.this);
        Calendar c=Calendar.getInstance();
        c.set(Calendar.HOUR_OF_DAY,9);
        c.set(Calendar.MINUTE,05);
        assesmentDatabaseHandler.addAssessment(StudentAssessment.builder().assessmentId("3").assesmentName("Class Test").moduleId("COMP101").room("P1159").scheduledDate(c.getTime()).weight(0.3).build());

        c.set(Calendar.HOUR_OF_DAY,9);
        c.set(Calendar.MINUTE,05);
        c.set(2018,4,20);
        assesmentDatabaseHandler.addAssessment(StudentAssessment.builder().assessmentId("4").assesmentName("Class Test 1").moduleId("COMP101").room("P1159").scheduledDate(c.getTime()).weight(0.3).build());

        c.set(Calendar.HOUR_OF_DAY,9);
        c.set(Calendar.MINUTE,05);
        c.set(2018,4,19);
        assesmentDatabaseHandler.addAssessment(StudentAssessment.builder().assessmentId("5").assesmentName("Class Test 2").moduleId("COMP101").room("P1159").scheduledDate(c.getTime()).weight(0.3).build());


    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_chat_conversation);

        Bundle extras = getIntent().getExtras();
        assesmentDatabaseHandler= new AssesmentDatabaseHandler(getApplicationContext());

//        init();





        TextView currentPerformance = (TextView) findViewById(R.id.currentGPA);
        TextView onesie = (TextView) findViewById(R.id.onesie);
        currentPerformance.setText(extras.get("gpa").toString());
        TextView note = (TextView) findViewById(R.id.module_note);
        if (extras.get("note")!=null){
            note.setText(extras.get("note").toString());
            note.setLayoutParams(onesie.getLayoutParams());

        } else {
            note.setText("");
        }

        moduleId=extras.get("id").toString();

        String[] tags = {"a","b","c"};

        LinearLayout linearLayout = (LinearLayout) findViewById(R.id.tags_list);
        for( int i = 0; i < tags.length; i++ )
        {
            TextView textView = new TextView(this);
            textView.setText(tags[i]);
            linearLayout.addView(textView);
        }



        final List<EventDay> events = new ArrayList<>();

        List<StudentAssessment> studentAssessments=assesmentDatabaseHandler.getByModuleId(moduleId);
        for (int i=0;i<studentAssessments.size();i++){

            Calendar cal = Calendar.getInstance();
            cal.setTime(studentAssessments.get(i).getScheduledDate());
            events.add(new EventDay(cal, R.drawable.stamp));
        }

//        Calendar calendar = Calendar.getInstance();
//        events.add(new EventDay(calendar, R.drawable.logo));

        CalendarView calendarView = (CalendarView) findViewById(R.id.calendarView);
        calendarView.setEvents(events);
        calendarView.setOnDayClickListener(new OnDayClickListener() {
            @Override
            public void onDayClick(EventDay eventDay) {
                if (events.contains(eventDay)){
                    ViewAssessmentFragment viewAssessmentFragment=new ViewAssessmentFragment();

                    Bundle args = new Bundle();
                    args.putString("title", "CA Tittle: Class Test");
                    args.putString("weight", "CA Weight: 10%");
                    args.putString("score", "CA Score: To be taken");
                    viewAssessmentFragment.setArguments(args);
                    viewAssessmentFragment.show(getSupportFragmentManager(), "New Chat Fragment");

                }
            }
        });

    }
    String id;



}










