package com.example.minht.madabot.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor(suppressConstructorProperties = true)
@NoArgsConstructor
@Builder
@ToString
public class StudentAssessment {

    private String moduleId;
    private String assessmentId;
    private String assesmentName;
    private String room;
    private Date scheduledDate;
    private Date takenDate;
    private double weight;
    private double score;


}
