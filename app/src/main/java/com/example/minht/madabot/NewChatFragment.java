package com.example.minht.madabot;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.NotificationCompat;
import android.util.Log;
import android.view.ContextThemeWrapper;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.example.minht.madabot.databasehandlers.AssesmentDatabaseHandler;
import com.example.minht.madabot.databasehandlers.ModuleDatabaseHandler;
import com.example.minht.madabot.model.StudentAssessment;
import com.example.minht.madabot.model.StudentModule;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.net.URL;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by minht on 11/12/2017.
 */

public class NewChatFragment extends DialogFragment {
    ModuleDatabaseHandler moduleDatabaseHandler ;
    AssesmentDatabaseHandler assesmentDatabaseHandler ;
    ArrayList<String> modulesIds;
    ArrayList<String> assessmentIds;

    String mId="";
    String asId="";
    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the Builder class for convenient dialog construction
        AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(getActivity(), R.style.AlertDialogCustom));
        // Get the layout inflater
        final LayoutInflater inflater = getActivity().getLayoutInflater();
        moduleDatabaseHandler = new ModuleDatabaseHandler(getActivity());
        // Inflate and set the layout for the dialog
        // Pass null as the parent view because its going in the dialog layout
        List<StudentModule> modules = moduleDatabaseHandler.getAllTopic();

        List<String> modulesNames =new ArrayList<>();

        modulesIds =new ArrayList<>();
        for (StudentModule m:modules){
            modulesNames.add(m.getModuleName());
            modulesIds.add(m.getModuleId());

        }
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this.getContext(),R.layout.spinner_item,modulesNames);


        final View v= inflater.inflate(R.layout.add_a_result,null);

        Spinner subjects= (Spinner) v.findViewById(R.id.subject_spinner);
        adapter.setDropDownViewResource(R.layout.spinner_item);
        subjects.setAdapter(adapter);

        TextView temp=(TextView) v.findViewById(R.id.temp);

        subjects.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                String modId=modulesIds.get(position);
                assesmentDatabaseHandler = new AssesmentDatabaseHandler(getActivity());
//                Toast.makeText(getContext(),modId, Toast.LENGTH_SHORT).show();
                Calendar c=Calendar.getInstance();
                c.set(Calendar.HOUR_OF_DAY,9);
                c.set(Calendar.MINUTE,05);


                List<StudentAssessment> asses = assesmentDatabaseHandler.getByModuleId(modId);
                if (asses==null){
                    asses=new ArrayList<>();
                }
                List<String> modulesNames =new ArrayList<>();

                if (asses.size()>0) assessmentIds=new ArrayList<>();
                for (StudentAssessment m:asses){
                    modulesNames.add(m.getAssesmentName());
                    assessmentIds.add(m.getAssessmentId());
                }
                if (modulesNames.size()>0) {
                    ArrayAdapter<String> adapter = new ArrayAdapter<String>(getContext(), R.layout.spinner_item, modulesNames);
                    Spinner subjects= (Spinner) v.findViewById(R.id.ca_spinner);
                    adapter.setDropDownViewResource(R.layout.spinner_item);
                    subjects.setAdapter(adapter);
                }

                mId=modId;



            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }

        });

// ca enter here
        Spinner ca= (Spinner) v.findViewById(R.id.ca_spinner);
        ca.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view, int position, long id) {
                asId=assessmentIds.get(position);
                Toast.makeText(getContext(),asId, Toast.LENGTH_SHORT).show();

            }

            @Override
            public void onNothingSelected(AdapterView<?> parent) {

            }
        });








        builder.setView(v)
                // Add action buttons
                .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int id) {
                        // sign in the user ...


                        TextView textView = (TextView) v.findViewById(R.id.score);
                        if (textView.getText().toString().equals("")){

                            Toast.makeText(getContext(),"the score is blank", Toast.LENGTH_SHORT).show();

                        } else {
                            String s=mId;
                            String a=asId;
                            String score= textView.getText().toString();

                            Intent i=new Intent(Intent.ACTION_VIEW);


                            new MyTask().execute(s, a, score);

                            assesmentDatabaseHandler.update(StudentAssessment.builder().assessmentId(asId).score(Double.parseDouble(score)).build());



                        }
                    }
                })
                .setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        NewChatFragment.this.getDialog().cancel();
                    }
                });

        return builder.create();
    }


    private class MyTask extends AsyncTask<String, Integer, String> {
        protected String doInBackground(String... arr) {
            String url="http://192.168.43.221:8888/result/1";
//            Toast.makeText(getContext(),"Thank you for submitting your result"+arr[0]+" "+arr[1], Toast.LENGTH_SHORT).show();
            MediaType JSON
                    = MediaType.parse("application/json; charset=utf-8");

            JSONObject message = new JSONObject();
            try {
                message.put("assessmentId",arr[1]);
                message.put("moduleId",arr[0]);
                message.put("studentId",100);
                message.put("scheduledDate","2018-03-03");
                message.put("takenDate", "2018-03-03");
                message.put("result", Double.parseDouble(arr[2]));
                Log.d("score",""+Double.parseDouble(arr[2]));
                message.put("takenDate", "2018-03-03");
                message.put("weight", 0.5);
                message.put("device", MyFirebaseInstanceIDService.getToken());

            } catch (JSONException e) {
                e.printStackTrace();
            }

            OkHttpClient client = new OkHttpClient();

            RequestBody body = RequestBody.create(JSON, message.toString());
            Request request = new Request.Builder()
                    .url(url)
                    .post(body)
                    .build();
            try {
                Response response = client.newCall(request).execute();

                response.body().string();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return "";
        }

        protected void onProgressUpdate(Integer... progress) {
        }

        protected void onPostExecute(Long result) {
        }
    }



}
