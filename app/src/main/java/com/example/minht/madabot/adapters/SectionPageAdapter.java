package com.example.minht.madabot.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by minht on 11/11/2017.
 */

public class SectionPageAdapter extends FragmentPagerAdapter{


    private final List<Fragment> fragments = new ArrayList<>();
    private final List<String> fragmenTitles= new ArrayList<>();

    public SectionPageAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return fragmenTitles.get(position);
    }

    @Override
    public Fragment getItem(int position) {
        return fragments.get(position);

    }

    @Override
    public int getCount() {
        return fragments.size();
    }

    public void addFragment(Fragment f, String tab1){
        fragments.add(f);
        fragmenTitles.add(tab1);
    }
}
