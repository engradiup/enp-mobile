package com.example.minht.madabot;

import android.app.AlertDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.provider.AlarmClock;
import android.support.v4.content.LocalBroadcastManager;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.loopj.android.http.AsyncHttpClient;
import com.loopj.android.http.AsyncHttpResponseHandler;
import com.loopj.android.http.RequestParams;

import cz.msebera.android.httpclient.Header;

import static com.github.mikephil.charting.charts.Chart.LOG_TAG;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);


        ProgressBar spinner = (ProgressBar) findViewById(R.id.startProgress);

        AsyncHttpClient client = new AsyncHttpClient();

        Intent i = new Intent(AlarmClock.ACTION_SET_ALARM);
        i.putExtra(AlarmClock.EXTRA_HOUR, 10);
        i.putExtra(AlarmClock.EXTRA_MINUTES, 20);
        i.putExtra(AlarmClock.EXTRA_MESSAGE,"Class test on Monday");
        startActivity(i);



        Intent intent = new Intent(MainActivity.this, LoginActivity.class);
                startActivity(intent);
    }

    @Override
    protected void onPause() {
        super.onPause();

        // unregister notification receiver
    }

    @Override
    protected void onResume() {
        super.onResume();

        // register notification receiver

    }



}
