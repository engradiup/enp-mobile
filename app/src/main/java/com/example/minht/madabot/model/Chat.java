package com.example.minht.madabot.model;

import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Created by minht on 11/12/2017.
 */



@Data
@NoArgsConstructor
@AllArgsConstructor(suppressConstructorProperties = true)
@Builder(builderMethodName = "builder")
public class Chat {

    private String id;
    private String topic_id;
    private String line;
    private String from;
    private String media;
    private Date dateSent;

    public Chat(String line, String from, String media){
        this.line=line;
        this.from=from;
        this.media=media;

    }

}
