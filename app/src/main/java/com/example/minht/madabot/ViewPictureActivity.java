package com.example.minht.madabot;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.ColorDrawable;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;

public class ViewPictureActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        setContentView(R.layout.activity_view_picture);
        Bundle extras = getIntent().getExtras();
        ImageView imageView = (ImageView) findViewById(R.id.bigimg);
        if (extras.getString("src")!=null && Integer.parseInt(extras.getString("src"))!=(0)){
        imageView.setImageResource(Integer.parseInt(extras.getString("src")));}else
        {
            imageView.setImageResource(R.drawable.cheatsheet);
        }
        if (extras.getByteArray("bitmap")!=null){
            byte[] byteArray = extras.getByteArray("bitmap");

            Bitmap bmp = BitmapFactory.decodeByteArray(byteArray, 0, byteArray.length);
            imageView.setImageBitmap(bmp);

        }
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        ColorDrawable colorDrawable = new ColorDrawable(Color.parseColor("#012C40"));
        getSupportActionBar().setBackgroundDrawable(colorDrawable);
        getSupportActionBar().setTitle("Scaled Image");
    }
}
