package com.example.minht.madabot.databasehandlers;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.example.minht.madabot.model.Chat;
import com.example.minht.madabot.model.StudentAssessment;
import com.example.minht.madabot.model.StudentModule;
import com.example.minht.madabot.model.Topic;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

/**
 * Created by minht on 12/9/2017.
 */

public class ModuleDatabaseHandler extends SQLiteOpenHelper {
    private static final int DATABASE_VERSION = 6

            ;

    private static final String DATABASE_NAME = "enp";

    private static final String KEY_ID = "moduleId";
    private static final String KEY_NAME = "moduleName";
    private static final String KEY_LECTURER = "lecturerName";
    private static final String KEY_TAGS = "tags";
    private static final String KEY_NOTE = "note";
    private static final String KEY_GPA = "gpa";

    private static final String KEY_ASS_ID = "assessmentId";
    private static final String KEY_MODULE_ID = "moduleId";
    private static final String KEY_ASS_NAME = "assesmentName";
    private static final String KEY_ROOM = "room";
    private static final String KEY_SCHEDULED = "scheduledDate";
    private static final String KEY_TAKEN = "takenDate";
    private static final String KEY_WEIGHT = "weight";
    private static final String KEY_SCORE = "score";




    public ModuleDatabaseHandler(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        String CREATE_MODULE_TABLE = "CREATE TABLE studentModule("
                + KEY_ID + " TEXT PRIMARY KEY," + KEY_NAME + " TEXT,"
                + KEY_LECTURER + " TEXT, " + KEY_TAGS+ " TEXT,"+KEY_NOTE+ " TEXT,"+KEY_GPA+ " REAL"+")";
        db.execSQL(CREATE_MODULE_TABLE);


        String CREATE_ASSESSMENT_TABLE = "CREATE TABLE assessments("
                + KEY_ASS_ID + " TEXT PRIMARY KEY, "
                + KEY_MODULE_ID + " TEXT, "
                + KEY_ASS_NAME + " TEXT, "
                + KEY_ROOM + " TEXT, "
                + KEY_SCHEDULED + " TEXT, "
                + KEY_TAKEN + " TEXT, "
                + KEY_WEIGHT + " REAL, "
                + KEY_SCORE + " REAL "
                +")";
        db.execSQL(CREATE_ASSESSMENT_TABLE);



    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        // Drop older table if existed
        db.execSQL("DROP TABLE IF EXISTS studentModule" );
        db.execSQL("DROP TABLE IF EXISTS assessments" );

        // Create tables again
        onCreate(db);
    }

    public List<StudentModule> getAllTopic(){


        List<StudentModule> moduleList = new ArrayList<>();
        // Select All Query
        String selectQuery = "SELECT  * FROM studentModule";

        SQLiteDatabase db = this.getWritableDatabase();
        Cursor cursor = db.rawQuery(selectQuery, null);

        // looping through all rows and adding to list
        if (cursor.moveToFirst()) {
            do {
                StudentModule module = new StudentModule();
                module.setModuleId(cursor.getString(0));
                module.setModuleName(cursor.getString(1));
                module.setLecturerName(cursor.getString(2));
                module.setTags(cursor.getString(3).split(","));
                module.setNote(cursor.getString(4));
                module.setCurrentGPA(Double.parseDouble(cursor.getString(5)));
                // Adding contact to list
                moduleList.add(module);
            } while (cursor.moveToNext());
        }

        // return contact list
        return moduleList;
    }

        public  void addModule(StudentModule module) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_ID, module.getModuleId());
        values.put(KEY_NAME, module.getModuleName());
        values.put(KEY_LECTURER, module.getLecturerName());
        values.put(KEY_TAGS, getString(module.getTags()));
        values.put(KEY_NOTE, module.getNote());
        values.put(KEY_GPA, module.getCurrentGPA());
        // Inserting Row
        db.insert("studentModule", null, values);
        db.close(); // Closing database connection
    }

    String getString(String[] s){
        String temp="";
        if (s != null && s.length>0 ){
            for (String st:s){
                temp=temp+ (s+",");

            }
        } else {return "";}
        return temp.substring(0,temp.length()-1);
    }







//    Topic getTopic(int id){
//
//
//        SQLiteDatabase db = this.getReadableDatabase();
//
//        Cursor cursor = db.query("studentModule", new String[] { KEY_ID,
//                        KEY_NAME, KEY_NAME,KEY_DES,KEY_FILENAME }, KEY_ID + "=?",
//                new String[] { String.valueOf(id) }, null, null, null, null);
//        if (cursor != null)
//            cursor.moveToFirst();
//
//        Topic topic=Topic.builder().id(cursor.getString(0)).name(cursor.getString(1)).description(cursor.getString(2)).filename(cursor.getString(3)).build();
//        return topic;
//    }

//
//    public int updateTopic(Topic topic) {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        ContentValues values = new ContentValues();
//        values.put(KEY_NAME, topic.getName());
//        values.put(KEY_DES, topic.getDescription());
//        values.put(KEY_FILENAME, topic.getFilename());
//
//        // updating row
//        return db.update("topic", values, KEY_ID + " = ?",
//                new String[] { String.valueOf(topic.getId()) });
//    }
//
//    // Deleting single contact
//    public void deleteTopic(Topic topic) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        db.delete("topic", KEY_ID + " = ?",
//                new String[] { String.valueOf(topic.getId()) });
//        db.close();
//    }
//
//    public void deleteTopic(String id) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        db.delete("topic", KEY_ID + " = ?",
//                new String[] { id });
//        db.close();
//    }
//    public void deleteChats() {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        db.delete("chat","",null);
//    }
//    public void deleteChats(String topic_id) {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        db.delete("chat","topic_id =?",new String[] { topic_id });
//    }
//

//
//    public  void addChat(Chat chat) {
//        SQLiteDatabase db = this.getWritableDatabase();
//
//        ContentValues values = new ContentValues();
//        values.put("line", chat.getLine());
//        values.put("froms", chat.getFrom());
//        values.put("topic_id", chat.getTopic_id());
//        values.put("date_sent", chat.getDateSent().toString());
//        // Inserting Row
//        db.insert("chat", null, values);
//        db.close(); // Closing database connection
//    }
//    public List<Chat> getAllChatForId(String id){
//
//        SQLiteDatabase db = this.getWritableDatabase();
//
//
//        List<Chat> chatList = new ArrayList<>();
//        // Select All Query
//
//
//
//        String selectQuery = "SELECT  * FROM chat where topic_id =? ";
//        Cursor cursor = db.rawQuery(selectQuery, new String[] {id});
//
//
//        // looping through all rows and adding to list
//        if (cursor.moveToFirst()) {
//            do {
//                Chat c=Chat.builder().id(cursor.getString(0)).line(cursor.getString(1)).from(cursor.getString(2)).topic_id(cursor.getString(3)).dateSent(new Date(cursor.getString(4))).build();
//                // Adding contact to list id,line,from,topic_id,date_sent
//                chatList.add(c);
//            } while (cursor.moveToNext());
//        }
//
//        // return contact list
//        return chatList;
//    }
//
//    public List<Integer> getChatsLength(){
//
//        SQLiteDatabase db = this.getWritableDatabase();
//
//
//        List<Integer> chatList = new ArrayList<>();
//        // Select All Query
//
//
//
//        String selectQuery = "SELECT  count (*) FROM chat group by topic_id ";
//        Cursor cursor = db.rawQuery(selectQuery,null);
//
//
//        // looping through all rows and adding to list
//        if (cursor.moveToFirst()) {
//            do {
//                // Adding contact to list id,line,from,topic_id,date_sent
//                chatList.add(Integer.parseInt(cursor.getString(0)));
//            } while (cursor.moveToNext());
//        }
//
//        // return contact list
//        return chatList;
//    }

    public  void update(StudentModule studentModule) {
        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(KEY_GPA, studentModule.getCurrentGPA());
        // Inserting Row

        db.update("assessments",values,"assessmentId =?",new String[] { studentModule.getModuleId() });

        db.close(); // Closing database connection
    }





}
