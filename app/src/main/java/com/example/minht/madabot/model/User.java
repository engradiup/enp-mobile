package com.example.minht.madabot.model;


import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@AllArgsConstructor(suppressConstructorProperties = true)
@Builder
public class User {

    private String username;
    private String password;
    private double currentGPA;
    private String firstName;
    private String lastName;
    private String studentId;
    private String classGroup;

}