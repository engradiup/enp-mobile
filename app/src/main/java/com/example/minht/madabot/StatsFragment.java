package com.example.minht.madabot;

import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.example.minht.madabot.databasehandlers.ModuleDatabaseHandler;
import com.example.minht.madabot.model.StudentModule;
import com.jjoe64.graphview.*;
import com.jjoe64.graphview.helper.StaticLabelsFormatter;
import com.jjoe64.graphview.series.BarGraphSeries;
import com.jjoe64.graphview.series.DataPoint;
import com.jjoe64.graphview.series.LineGraphSeries;

import java.util.List;

/**
 * Created by minht on 11/11/2017.
 */

public class StatsFragment extends android.support.v4.app.Fragment {
    private static final String TAB1="TAB1";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        View view =inflater.inflate(R.layout.stats_fragment,container,false);
//        DatabaseHandler db = new DatabaseHandler(getActivity());
        ModuleDatabaseHandler db = new ModuleDatabaseHandler(getContext());
        List<StudentModule> module=db.getAllTopic();


        DataPoint[] dP= new DataPoint[module.size()];
        String[] label=new String[module.size()];

        for(int i=0;i<module.size();i++){

            dP[i] = new DataPoint((double) i,(double) (module.get(i).getCurrentGPA()));
            label[i]=module.get(i).getModuleId() == null ?"module" : module.get(i).getModuleId();
        }


        GraphView graph1 = (GraphView) view.findViewById(R.id.graph1);
        StaticLabelsFormatter staticLabelsFormatter = new StaticLabelsFormatter(graph1);
        staticLabelsFormatter.setHorizontalLabels(label);

        BarGraphSeries<DataPoint> series1 = new BarGraphSeries<>(dP);

        graph1.setBackgroundColor(Color.WHITE);
        graph1.setTitleColor(Color.parseColor("#E21738"));

        graph1.setTitle("Result by Modules");

        graph1.addSeries(series1);
        series1.setValuesOnTopColor(Color.RED);
        series1.setColor(Color.parseColor("#041A38"));
        series1.setDrawValuesOnTop(true);


        series1.setDataWidth(0.5);
                staticLabelsFormatter.setVerticalLabels(new String[] {"0", "20", "40","60","80","100"});
        graph1.getGridLabelRenderer().setLabelFormatter(staticLabelsFormatter);
        graph1.getGridLabelRenderer().setVerticalLabelsVisible(true);
        graph1.getGridLabelRenderer().setHorizontalLabelsVisible(true);
        graph1.getGridLabelRenderer().setVerticalLabelsColor(Color.BLACK);
        graph1.getGridLabelRenderer().setHorizontalLabelsColor(Color.BLACK);
        graph1.getViewport().setYAxisBoundsManual(true);
        graph1.getViewport().setMinY(0);
        graph1.getViewport().setMaxY(100);
        return view;
    }
}
