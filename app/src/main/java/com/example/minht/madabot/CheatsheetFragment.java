package com.example.minht.madabot;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * Created by minht on 11/11/2017.
 */

public class CheatsheetFragment extends Fragment{

    private static final String TAB1="TAB1";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view =inflater.inflate(R.layout.cheatsheet_fragment,container,false);
        return view;

    }
}
